
public class DrawingMaster
{
    BackGround BackGround = new BackGround();
    MessageBox MessageBox = new MessageBox();
    Button Button = new Button();
    ScheduleController ScheduleController = new ScheduleController();
    ClockModel ClockModel = new ClockModel();
    Weather Weather = new Weather();
    //private int SerifNumber = 11;
    public DrawingMaster() 
    {
        /*
        BackGround.ShowBackGround();
        MessageBox.ShowBox();
        */
    }
    private void UpdateDrawing(int modenumber)
    {
        BackGround.ShowBackGround();
        Button.ShowButton(modenumber);
        MessageBox.ShowBox("藍月 雪亜");
        //MessageBox.ShowSerif(SerifNumber);
        switch (modenumber) 
        {
            case 11:
                MessageBox.ShowSerif("おはようございます");
                break;
            case 14:
                MessageBox.ShowSerif("何か御用ですか…？"); 
                break;
            case 15:
                MessageBox.ShowSerif("何か御用ですか…？"); 
                break;
            case 21:
                String Serif = ClockModel.Show();
                MessageBox.ShowSerif(Serif);
                break;
            case 31:
                MessageBox.ShowSerif("どのカテゴリーでしょうか？");
                break;  
            case 32:
                MessageBox.ShowSerif("TODOをお見せしますね");
                ScheduleController.ShowSchedules(modenumber);
                break;
            case 33:
                MessageBox.ShowSerif("通知をお見せしますね");
                ScheduleController.ShowSchedules(modenumber);
                break;
            case 34:
                MessageBox.ShowSerif("締め切りをお見せしますね");
                ScheduleController.ShowSchedules(modenumber);
                break;
            case 35:
                MessageBox.ShowSerif("スケジュールをお見せしますね");
                ScheduleController.ShowSchedules(modenumber);
                break;
                /*
            case 36:
                MessageBox.ShowSerif("～のスケジュールです");
                break;
                */
            case 41:
                MessageBox.ShowSerif("分かりました。\nスケジュールを記憶しますね。");
                break;
            case 42:
                MessageBox.ShowSerif("キーワードを教えて下さい");
                ScheduleController.ShowKeyword();
                break;
            case 43:
                MessageBox.ShowSerif("開始時間は入力しますか？\n開始時間を入力すると、通知・スケジュール、\n開始時間を入力しないと、TODO・締め切り\nのいずれかで記憶します");
                ScheduleController.ShowSelectButton();
                break;
                case 44:
                    MessageBox.ShowSerif("開始時間を教えて下さい");
                    ScheduleController.ShowInputView();
                    break;
                case 45:
                    MessageBox.ShowSerif("分かりました。終了時間は入力しますか？\n終了時間を入力すると、スケジュール、\n終了時間を入力しないと、通知として記憶します");
                    ScheduleController.ShowSelectButton();
                    break;
                    case 46:
                        MessageBox.ShowSerif("終了時間を教えて下さい");
                        ScheduleController.ShowInputView();
                        break;
                    case 47:
                        MessageBox.ShowSerif("分かりました。スケジュールとして記憶しておきますね");
                        break;
                case 48:
                    MessageBox.ShowSerif("分かりました。通知として記憶しておきますね");
                    break;
            case 49:
                MessageBox.ShowSerif("分かりました。終了時間は入力しますか？\n終了時間を入力すると、締め切り、\n開始時間を入力しないと、TODOとして記憶します");
                ScheduleController.ShowSelectButton();
                break;
                case 50:
                    MessageBox.ShowSerif("終了時間を教えて下さい");
                    ScheduleController.ShowInputView();
                    break;
                case 51:
                    MessageBox.ShowSerif("分かりました。締め切りとして記憶しておきますね");
                    break;
                case 52:
                    MessageBox.ShowSerif("分かりました。TODOとして記憶しておきますね");
                    break;
                case 61:
                    MessageBox.ShowSerif("ええと……今日の函館市の天気は"+ Weather.Telop + "です。\n最高気温は" + Weather.Max + "度です");
                    break;
                
        }
    }
    public int mousePressedEventListener(int modenumber)
    {
        switch (modenumber) 
        {
            case 11:
                return 14;
            case 12:
                return 14;
            case 13:
                return 14;
            case 14:
                return 15;
            case 15:
                return Button.mousePressedEventListener(modenumber);
            case 21:
                return 15;
            case 31:
                return Button.mousePressedEventListener(modenumber);
            case 41:
                return 42;
            case 42:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 43:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 44:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 45:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 46:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 47:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 48:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 49:
                return ScheduleController.mousePressedEventListener(modenumber);
            case 50:
                return ScheduleController.mousePressedEventListener(modenumber);
        }
        return 11;
    }
    public void Dispose()
    {
        this.ScheduleController.WriteJSON();
    }
}