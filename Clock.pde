import java.util.Calendar;

public class ClockModel
{
    PImage clockboard = loadImage("resized_clock.png");
    PImage short_hand = loadImage("short_hand.png");
    PImage long_hand = loadImage("long_hand.png");
    PImage second_hand = loadImage("second_hand.png");
    
    public ClockModel()
    {
        short_hand.resize(0,180);
        long_hand.resize(0,260);
        second_hand.resize(0,300);
    }
    public String Show()
    {
        pushMatrix();
        String[] week_name = {"日曜日", "月曜日", "火曜日", "水曜日", 
                          "木曜日", "金曜日", "土曜日"};

        fill(0,0,255,32);
        noStroke();
        rect(150,120,300,300);
        Calendar calendar = Calendar.getInstance();
        int year   = calendar.get(Calendar.YEAR);
        int month  = calendar.get(Calendar.MONTH) + 1;
        int day    = calendar.get(Calendar.DATE);
        int hour   = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int week   = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        image(clockboard, 150, 120, 300, 300);
        //pushMatrix();
        
        translate(300, 270); //②
        imageMode(CENTER);
        pushMatrix();
        rotate(TWO_PI / 60 * minute); //③
        image(long_hand, 0, 0); //⑤
        popMatrix();
        pushMatrix();
        rotate(TWO_PI / 12 * (hour % 12)); //③
        image(short_hand, 0, 0); //⑤
        popMatrix();
        rotate(TWO_PI / 60 * second); //③
        image(second_hand, 0, 0); //⑤
        //popMatrix();
        imageMode(CORNER);
        
        String Serif = "ええと……今は……" + "\n" +
                        year + "年" + month + "月" + day + "日 " + week_name[week] + "\n" +
                        hour + "時" + minute + "分" + second + "秒です" + "\n" +
                        "時計…変じゃないです…か…？";
        popMatrix();
        return Serif;
    }
}
