public class Button 
{
    PImage NomalButtonImage = loadImage("button_blue.png");
    PImage FlashButtonImage = loadImage("button_blue_hakkou.png");
    public Button() 
    {
        
    }
    public void ShowButton(int modenumber)
    {
        switch (modenumber) 
        {
            case 15:
                textAlign(CENTER,CENTER);
                textSize(20);
                if(IsMouseOnButton(50))
                {
                    image(FlashButtonImage, width / 2 - 133, 50, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 50, 267, 77);
                }
                text("時計を見せてもらう",width / 2, (50 + 127) / 2);
                if(IsMouseOnButton(150))
                {
                    image(FlashButtonImage, width / 2 - 133, 150, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 150, 267, 77);
                }
                text("スケジュールを聞く",width / 2, (150 + 227) / 2);
                if(IsMouseOnButton(250))
                {
                    image(FlashButtonImage, width / 2 - 133, 250, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 250, 267, 77);
                }
                text("スケジュールを覚えてもらう",width / 2, (250 + 327) / 2);
                if(IsMouseOnButton(350))
                {
                    image(FlashButtonImage, width / 2 - 133, 350, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 350, 267, 77);
                }
                text("お天気を聞く",width / 2, (350 + 427) / 2);
                break;
            case 31:
                textAlign(CENTER,CENTER);
                textSize(20);
                if(IsMouseOnButton(50))
                {
                    image(FlashButtonImage, width / 2 - 133, 50, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 50, 267, 77);
                }
                text("TODOを聞く",width / 2, (50 + 127) / 2);
                if(IsMouseOnButton(150))
                {
                    image(FlashButtonImage, width / 2 - 133, 150, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 150, 267, 77);
                }
                text("通知を聞く",width / 2, (150 + 227) / 2);
                if(IsMouseOnButton(250))
                {
                    image(FlashButtonImage, width / 2 - 133, 250, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 250, 267, 77);
                }
                text("締め切りを聞く",width / 2, (250 + 327) / 2);
                if(IsMouseOnButton(350))
                {
                    image(FlashButtonImage, width / 2 - 133, 350, 267, 77);
                }
                else 
                {
                    image(NomalButtonImage, width / 2 - 133, 350, 267, 77);
                }
                text("スケジュールを聞く",width / 2, (350 + 427) / 2);
                break;
        }
    }
    private boolean IsMouseOnButton(int buttonY)
    {
        int UpButtonY = (buttonY + (buttonY + 77)) / 2 - (77 / 4);
        int DownButtonY = (buttonY + (buttonY + 77)) / 2 + (77 / 4);
        int LeftButtonX = width / 2 - 120;
        int RightButtonX = width / 2 + 120;
        if(mouseX > LeftButtonX && mouseX < RightButtonX)
        {
            if(mouseY > UpButtonY && mouseY < DownButtonY)
            {
                return true;
            }
        }
        int buttonX = width / 2;
        return false;
    }
    public int mousePressedEventListener(int modenumber)
    {
        switch (modenumber) 
        {
            case 15:
                if(IsMouseOnButton(50))
                {
                    return 21;
                }
                if(IsMouseOnButton(150))
                {
                    return 31;
                }
                if(IsMouseOnButton(250))
                {
                    return 41;
                }
                if(IsMouseOnButton(350))
                {
                    return 61;
                }
                break;
            case 31:
                if(IsMouseOnButton(50))
                {
                    return 32;
                }
                if(IsMouseOnButton(150))
                {
                    return 33;
                }
                if(IsMouseOnButton(250))
                {
                    return 34;
                }
                if(IsMouseOnButton(350))
                {
                    return 35;
                }
                break;
            
        }
        return modenumber;
    }
}
