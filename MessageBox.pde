public class MessageBox 
{
    PImage serifframe = loadImage("box_blue_name.png");
    PFont font22 = loadFont("HGPMinchoE-22.vlw");
    
    public MessageBox() 
    {
        
    }
    public void ShowBox(String name)
    {
        image(serifframe, 0, height - 169, 480, 169);
        textAlign(CENTER,CENTER);
        textFont(font22, 22);
        fill(255,255,239);
        text(name, 65, 490);
    }
    public void ShowSerif(String serif)
    {
        stroke(153);
        fill(255,255,239);
        textFont(font22, 22);
        textAlign(LEFT, TOP); 
        text(serif, 20, 525);
        if(frameCount % 60 > 30)
        {
            textAlign(CENTER, CENTER); 
            text("▼", 450, 610);
        }
    }
}
