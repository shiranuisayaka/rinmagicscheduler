import java.util.Calendar;

public class ScheduleController
{
    private PFont font16 = loadFont("HGPGothicE-16.vlw");
    private PFont font24 = loadFont("SourceCodePro-Semibold-24.vlw");
    
    private ArrayList<ScheduleModel> Schedules = new ArrayList<ScheduleModel>();
    private String[] KeyWords;
    private int CurrentKeyWords = 0;

    PImage NomalButtonImage_Big = loadImage("button_blue.png");
    PImage FlashButtonImage_Big = loadImage("button_blue_hakkou.png");

    PImage NomalButtonImage_Small = loadImage("button_blue.png");
    PImage FlashButtonImage_Small = loadImage("button_blue_hakkou.png");

    PImage NomalButtonImage = loadImage("button_blue.png");
    PImage FlashButtonImage = loadImage("button_blue_hakkou.png");

    private java.util.Calendar NowCalendar = Calendar.getInstance();
    private int Year   = this.NowCalendar.get(Calendar.YEAR);

    private int Month  = this.NowCalendar.get(Calendar.MONTH) + 1;
    private int PrevMonth = this.Month - 1;
    private int NextMonth = this.Month + 1;
    private int Day    = this.NowCalendar.get(Calendar.DATE);
    private int PrevDay = this.Day - 1;
    private int NextDay = this.Day + 1;
    private int Hour   = this.NowCalendar.get(Calendar.HOUR_OF_DAY);
    private int PrevHour = this.Hour - 1;
    private int NextHour = this.Hour + 1;
    private int Minute = this.NowCalendar.get(Calendar.MINUTE);
    private int PrevMinute = this.Minute - 1;
    private int NextMinute = this.Minute + 1;

    public ScheduleController()
    {
        this.KeyWords = loadStrings("Keywords.txt");
        this.NomalButtonImage_Big.resize(240,0);
        this.FlashButtonImage_Big.resize(300,0);
        this.NomalButtonImage_Small.resize(145,0);
        this.FlashButtonImage_Small.resize(165,0);

        try 
        {
            processing.data.JSONArray values = loadJSONArray("schedules.json");
            for (int i = 0; i < values.size(); i++) 
            {
                processing.data.JSONObject schedule = values.getJSONObject(i); 

                String starttime = schedule.getString("starttime");
                String endtime = schedule.getString("endtime");
                String content = schedule.getString("content");

                Schedules.add(new ScheduleModel(starttime,endtime,content));
            }
        } 
        catch (Exception e) 
        {
            
        } 
        finally 
        {
            
        }

        
        //testmodel = new ScheduleModel("0100", "0400","つむぎちゃんをめでる");
    }
    
    public void ShowSchedules(int modenumber)
    {
        noStroke();
        fill(0, 128);
        rect(160, 60, 300, 340);

        fill(255,255,239);
        textFont(font16, 16);
        textAlign(LEFT, UP);
        int count = 0;
        for(int i = 0; i < this.Schedules.size(); i++)
        {
            if(modenumber == 32 && this.Schedules.get(i).Category == 0)
            {
                text(this.Schedules.get(i).GetShowScheduleString(), 170, 80 + count++ * 20);
            }
            if(modenumber == 33 && this.Schedules.get(i).Category == 1)
            {
                text(this.Schedules.get(i).GetShowScheduleString(), 170, 80 + count++ * 60);
            }
            if(modenumber == 34 && this.Schedules.get(i).Category == 2)
            {
                text(this.Schedules.get(i).GetShowScheduleString(), 170, 80 + count++ * 60);
            }
            if(modenumber == 35 && this.Schedules.get(i).Category == 3)
            {
                text(this.Schedules.get(i).GetShowScheduleString(), 170, 80 + count++ * 60);
            }
        }
    }
    public void ShowKeyword()
    {
        imageMode(CENTER);

        textAlign(CENTER,CENTER);
        int previousKeyWord =
                        this.CurrentKeyWords == 0 ?
                        this.KeyWords.length - 1 : this.CurrentKeyWords - 1;
        int nextKeyWord = 
                        this.CurrentKeyWords == this.KeyWords.length - 1 ?
                        0 : this.CurrentKeyWords + 1;
        image(NomalButtonImage_Big, width / 2, 100);
        text(this.KeyWords[previousKeyWord], width / 2, 100);
        image(FlashButtonImage_Big, width / 2, 200);
        text(this.KeyWords[this.CurrentKeyWords], width / 2, 200);
        image(NomalButtonImage_Big, width / 2, 300);
        text(this.KeyWords[nextKeyWord], width /2, 300);

        imageMode(CORNER);
    }
    public void ShowSelectButton()
    {
        textAlign(CENTER,CENTER);
        if(IsMouseOnButton_TwoButtons(150))
        {
            image(FlashButtonImage, width / 2 - 133, 150, 267, 77);
        }
        else 
        {
            image(NomalButtonImage, width / 2 - 133, 150, 267, 77);
        }
        text("はい",width / 2, (150 + 227) / 2);
        if(IsMouseOnButton_TwoButtons(250))
        {
            image(FlashButtonImage, width / 2 - 133, 250, 267, 77);
        }
        else 
        {
            image(NomalButtonImage, width / 2 - 133, 250, 267, 77);
        }
        text("いいえ",width / 2, (250 + 327) / 2);
    }
    public void ShowInputView()
    {
        imageMode(CENTER);

        textAlign(CENTER,CENTER);
        
        image(FlashButtonImage_Small,width * 0.18, 150);
        image(FlashButtonImage_Small,width * 0.5, 150);
        image(FlashButtonImage_Small,width * 0.82, 150);

        image(NomalButtonImage_Small,width * 0.18, 110);
        image(NomalButtonImage_Small,width * 0.5, 110);
        image(NomalButtonImage_Small,width * 0.82, 110);

        image(NomalButtonImage_Small,width * 0.18, 190);
        image(NomalButtonImage_Small,width * 0.5, 190);
        image(NomalButtonImage_Small,width * 0.82, 190);

        image(FlashButtonImage_Small,width * 0.5, 300);
        image(FlashButtonImage_Small,width * 0.82, 300);

        image(NomalButtonImage_Small,width * 0.5, 260);
        image(NomalButtonImage_Small,width * 0.82, 260);

        image(NomalButtonImage_Small,width * 0.5, 340);
        image(NomalButtonImage_Small,width * 0.82, 340);

        
        textAlign(CENTER,CENTER);
        textSize(24);
        fill(255,255,255,255);
        strokeWeight(1);
        text(this.Year - 1, width * 0.18, 110);
        text(this.Year + 1,width * 0.18, 190);
        text(this.PrevMonth, width * 0.5, 110);
        text(this.NextMonth, width * 0.5, 190);
        text(this.PrevDay, width * 0.82, 110);
        text(this.NextDay, width * 0.82, 190);
        text(this.PrevHour, width * 0.5, 260);
        text(this.NextHour, width * 0.5, 340);
        text(this.PrevMinute, width * 0.82, 260);
        text(this.NextMinute, width * 0.82, 340);

        textSize(32);
        text(this.Year, width * 0.18, 150);
        text(this.Month, width * 0.5, 150);
        text(this.Day, width * 0.82, 150);
        text(this.Hour, width * 0.5, 300);
        text(this.Minute, width * 0.82, 300);
        
        if(frameCount % 60 > 30)
        {
            fill(0);
            text("：", ((width * 0.5) + (width * 0.82)) / 2, 300);
        }
        imageMode(CORNER);

        if(IsMouseOnButton_TwoButtons(400))
        {
            image(FlashButtonImage, width / 2 - 133, 400, 267, 77);
        }
        else 
        {
            image(NomalButtonImage, width / 2 - 133, 400, 267, 77);
        }
        fill(255);
        text("伝える",width / 2, (400 + 477) / 2);

        /*
        textFont(font24);
        fill(0,192);
        rect(70, 40, width - 140, 390);

        strokeWeight(2);
        fill(220,220,220,64);

        rect(90,70, 80, 33);
        rect(80,120, 100, 60);
        rect(90,198, 80, 33);
        
        rect(200,70, 80, 33);
        rect(190,120, 100, 60);
        rect(200,198, 80, 33);

        rect(310,70, 80, 33);
        rect(300,120, 100, 60);
        rect(310,198, 80, 33);

        rect(80,290, 140, 65);
        rect(90,373, 120,40);

        rect(190,120, 100, 60);

        textAlign(CENTER,CENTER);
        textSize(24);
        fill(255,255,255,255);
        strokeWeight(1);
        text(this.Year - 1, (90 + (90 + 80)) / 2, (70 + (70 + 33)) / 2);
        text(this.Year + 1, (90 + (90 + 80)) / 2, (198 + (198 + 33)) / 2);
        text(this.Month - 1, (200 + (200 + 80)) / 2, (70 + (70 + 33)) / 2);
        text(this.Month + 1, (200 + (200 + 80)) / 2, (198 + (198 + 33)) / 2);
        text(this.Day - 1, (310 + (310 + 80)) / 2, (70 + (70 + 33)) / 2);
        text(this.Day + 1, (310 + (310 + 80)) / 2, (198 + (198 + 33)) / 2);
        
        textSize(32);
        text(this.Year, (80 + (80 + 100)) / 2, (120 + (120 + 60)) / 2);
        text(this.Month, (190 + (190 + 100)) / 2, (120 + (120 + 60)) / 2);
        text(this.Day, (300 + (300 + 100)) / 2, (120 + (120 + 60)) / 2);
        */
    }
    public int mousePressedEventListener(int modenumber)
    {
        switch (modenumber) 
        {
            case 42:
                int mousePotisionResult = this.IsMouseOnButton(modenumber);
                if(mousePotisionResult == 0)
                {
                    return modenumber;
                }
                if(mousePotisionResult == 2)
                {
                    return 43;
                }
                if(mousePotisionResult == 1)
                {
                    this.CurrentKeyWords = 
                        this.CurrentKeyWords == 0 ?
                        this.KeyWords.length - 1 : this.CurrentKeyWords - 1;
                    this.NowCalendar = Calendar.getInstance();
                    this.Year   = this.NowCalendar.get(Calendar.YEAR);
                    this.Month  = this.NowCalendar.get(Calendar.MONTH) + 1;
                    this.Day    = this.NowCalendar.get(Calendar.DATE);
                    this.Hour   = this.NowCalendar.get(Calendar.HOUR_OF_DAY);
                    this.Minute = this.NowCalendar.get(Calendar.MINUTE);
                    return modenumber;
                }
                if(mousePotisionResult == 3)
                {
                    this.CurrentKeyWords =
                        this.CurrentKeyWords == this.KeyWords.length - 1 ?
                        0 : this.CurrentKeyWords + 1;
                    return modenumber;
                }
            case 43:
                if(IsMouseOnButton_TwoButtons(150))
                {
                    this.PropertyChanged();
                    return 44;
                }
                if(IsMouseOnButton_TwoButtons(250))
                {
                    return 49;
                }
                return modenumber;
            case 44:
                if(mouseX >= width * 0.18 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.18 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Year--;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.18 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.18 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Year++;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Month = PrevMonth;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Month = NextMonth;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Day = PrevDay;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Day = NextDay;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 260 - NomalButtonImage_Small.height / 2 && mouseY <= 260 + NomalButtonImage_Small.height / 2)
                    {
                        this.Hour = PrevHour;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 340 - NomalButtonImage_Small.height / 2 && mouseY <= 340 + NomalButtonImage_Small.height / 2)
                    {
                        this.Hour = NextHour;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 260 - NomalButtonImage_Small.height / 2 && mouseY <= 260 + NomalButtonImage_Small.height / 2)
                    {
                        this.Minute = PrevMinute;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 340 - NomalButtonImage_Small.height / 2 && mouseY <= 340 + NomalButtonImage_Small.height / 2)
                    {
                        this.Minute = NextMinute;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(IsMouseOnButton_TwoButtons(400))
                {
                    String starttime = str(this.Year) + "/" + str(this.Month) + "/" + str(this.Day) + " " + str(this.Hour) + ":" + str(this.Minute);
                    this.Schedules.add(new ScheduleModel(starttime, "",KeyWords[CurrentKeyWords]));
                    return 45;
                }
                return modenumber;
            case 45:
                if(IsMouseOnButton_TwoButtons(150))
                {
                    this.PropertyChanged();
                    return 46;
                }
                if(IsMouseOnButton_TwoButtons(250))
                {
                    return 48;
                }
                return modenumber;
            case 46:
                if(mouseX >= width * 0.18 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.18 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Year--;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.18 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.18 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Year++;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Month = PrevMonth;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Month = NextMonth;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Day = PrevDay;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Day = NextDay;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 260 - NomalButtonImage_Small.height / 2 && mouseY <= 260 + NomalButtonImage_Small.height / 2)
                    {
                        this.Hour = PrevHour;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 340 - NomalButtonImage_Small.height / 2 && mouseY <= 340 + NomalButtonImage_Small.height / 2)
                    {
                        this.Hour = NextHour;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 260 - NomalButtonImage_Small.height / 2 && mouseY <= 260 + NomalButtonImage_Small.height / 2)
                    {
                        this.Minute = PrevMinute;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 340 - NomalButtonImage_Small.height / 2 && mouseY <= 340 + NomalButtonImage_Small.height / 2)
                    {
                        this.Minute = NextMinute;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(IsMouseOnButton_TwoButtons(400))
                {
                    String endtime = str(this.Year) + "/" + str(this.Month) + "/" + str(this.Day) + " " + str(this.Hour) + ":" + str(this.Minute);
                    ScheduleModel LastModel = this.Schedules.get(this.Schedules.size() - 1);
                    LastModel.EndTime = endtime;
                    LastModel.Category = 3;
                    try{
                    this.Schedules.remove(this.Schedules.size() - 1);
                    }finally{}
                    this.Schedules.add(LastModel);
                    return 47;
                }
                return modenumber;
            case 49:
                if(IsMouseOnButton_TwoButtons(150))
                {
                    this.PropertyChanged();
                    return 50;
                }
                if(IsMouseOnButton_TwoButtons(250))
                {
                    this.Schedules.add(new ScheduleModel("", "", KeyWords[CurrentKeyWords]));
                    return 52;
                }
                return modenumber;
            case 50:
                if(mouseX >= width * 0.18 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.18 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Year--;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.18 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.18 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Year++;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Month = PrevMonth;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Month = NextMonth;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 110 - NomalButtonImage_Small.height / 2 && mouseY <= 110 + NomalButtonImage_Small.height / 2)
                    {
                        this.Day = PrevDay;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 190 - NomalButtonImage_Small.height / 2 && mouseY <= 190 + NomalButtonImage_Small.height / 2)
                    {
                        this.Day = NextDay;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 260 - NomalButtonImage_Small.height / 2 && mouseY <= 260 + NomalButtonImage_Small.height / 2)
                    {
                        this.Hour = PrevHour;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.5 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.5 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 340 - NomalButtonImage_Small.height / 2 && mouseY <= 340 + NomalButtonImage_Small.height / 2)
                    {
                        this.Hour = NextHour;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 260 - NomalButtonImage_Small.height / 2 && mouseY <= 260 + NomalButtonImage_Small.height / 2)
                    {
                        this.Minute = PrevMinute;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(mouseX >= width * 0.82 - NomalButtonImage_Small.width / 2 && mouseX <= width * 0.82 + NomalButtonImage_Small.width / 2 &&
                    mouseY >= 340 - NomalButtonImage_Small.height / 2 && mouseY <= 340 + NomalButtonImage_Small.height / 2)
                    {
                        this.Minute = NextMinute;
                        this.PropertyChanged();
                        return modenumber;
                    }
                if(IsMouseOnButton_TwoButtons(400))
                {
                    String endtime = str(this.Year) + "/" + str(this.Month) + "/" + str(this.Day) + " " + str(this.Hour) + ":" + str(this.Minute);
                    this.Schedules.add(new ScheduleModel("", endtime, KeyWords[CurrentKeyWords]));
                    return 51;
                }
                return modenumber;
                /*
                    image(FlashButtonImage_Small,width * 0.18, 150);
                    image(FlashButtonImage_Small,width * 0.5, 150);
                    image(FlashButtonImage_Small,width * 0.82, 150);

                    image(NomalButtonImage_Small,width * 0.18, 110);
                    image(NomalButtonImage_Small,width * 0.5, 110);
                    image(NomalButtonImage_Small,width * 0.82, 110);

                    image(NomalButtonImage_Small,width * 0.18, 190);
                    image(NomalButtonImage_Small,width * 0.5, 190);
                    image(NomalButtonImage_Small,width * 0.82, 190);

                    image(FlashButtonImage_Small,width * 0.5, 300);
                    image(FlashButtonImage_Small,width * 0.82, 300);

                    image(NomalButtonImage_Small,width * 0.5, 260);
                    image(NomalButtonImage_Small,width * 0.82, 260);

                    image(NomalButtonImage_Small,width * 0.5, 340);
                    image(NomalButtonImage_Small,width * 0.82, 340);
                */


            /*
            case 41:
                this.NowCalendar = Calendar.getInstance();
                this.Year   = this.NowCalendar.get(Calendar.YEAR);
                this.Month  = this.NowCalendar.get(Calendar.MONTH) + 1;
                this.Day    = this.NowCalendar.get(Calendar.DATE);
                this.Hour   = this.NowCalendar.get(Calendar.HOUR_OF_DAY);
                this.Minute = this.NowCalendar.get(Calendar.MINUTE);
                return 42;
            */
        }
        return 11;
    }
    private int IsMouseOnButton(int modenumber)
    {
        switch(modenumber)
        {
            case 42:
                int UpButton_Up_Y = 100 - NomalButtonImage_Big.height / 2;
                int UpButton_Down_Y = 100 + NomalButtonImage_Big.height / 2;
                int UpButton_Left_X = width / 2 - NomalButtonImage_Big.width / 2;
                int UpButton_Right_X = width / 2 + NomalButtonImage_Big.width / 2;

                if(mouseX >= UpButton_Left_X && mouseX <= UpButton_Right_X 
                    && mouseY >= UpButton_Up_Y && mouseY <= UpButton_Down_Y)
                {
                    return 1;
                }

                int CenterButton_Up_Y = 200 - FlashButtonImage_Big.height / 2;
                int CenterButton_Down_Y = 200 + FlashButtonImage_Big.height / 2;
                int CenterButton_Left_X = width / 2 - FlashButtonImage_Big.width / 2;
                int CenterButton_Right_X = width / 2 + FlashButtonImage_Big.width / 2;

                if(mouseX >= CenterButton_Left_X && mouseX <= CenterButton_Right_X 
                    && mouseY >= CenterButton_Up_Y && mouseY <= CenterButton_Down_Y)
                {
                    return 2;
                }

                int DownButton_Up_Y = 300 - NomalButtonImage_Big.height / 2;
                int DownButton_Down_Y = 300 + NomalButtonImage_Big.height / 2;
                int DownButton_Left_X = width / 2 - NomalButtonImage_Big.width / 2;
                int DownButton_Right_X = width / 2 + NomalButtonImage_Big.width / 2;

                if(mouseX >= DownButton_Left_X && mouseX <= DownButton_Right_X 
                    && mouseY >= DownButton_Up_Y && mouseY <= DownButton_Down_Y)
                {
                    return 3;
                }
            break;  
        }
        return 0;
    }
    private void PropertyChanged() //三項演算子使う気力が無くなった
    {
        this.NextMonth = this.Month + 1;
        this.PrevMonth = this.Month - 1;
        this.NextDay = this.Day + 1;
        this.PrevDay = this.Day - 1;
        this.NextHour = this.Hour + 1;
        this.PrevHour = this.Hour - 1;
        this.NextMinute = this.Minute + 1;
        this.PrevMinute = this.Minute - 1;

        if(this.Month == 1)
        {
            PrevMonth = 12;
        }
        if(this.Month == 12)
        {
            NextMonth = 1;
        }
        if(this.Day == 1)
        {
            PrevDay = 31;
        }
        if(this.Day == 31)
        {
            NextDay = 1;
        }
        if(this.Hour == 0)
        {
            PrevHour = 23;
        }
        if(this.Hour == 23)
        {
            NextHour = 0;
        }
        if(this.Minute == 0)
        {
            PrevMinute = 59;
        }
        if(this.Minute == 59)
        {
            NextMinute = 0;
        }
    }
    private boolean IsMouseOnButton_TwoButtons(int buttonY)
    {
        int UpButtonY = (buttonY + (buttonY + 77)) / 2 - (77 / 4);
        int DownButtonY = (buttonY + (buttonY + 77)) / 2 + (77 / 4);
        int LeftButtonX = width / 2 - 120;
        int RightButtonX = width / 2 + 120;
        if(mouseX > LeftButtonX && mouseX < RightButtonX)
        {
            if(mouseY > UpButtonY && mouseY < DownButtonY)
            {
                return true;
            }
        }
        int buttonX = width / 2;
        return false;
    }
    public void WriteJSON()
    {
        processing.data.JSONArray values = new processing.data.JSONArray();
        for (int i = 0; i < Schedules.size(); i++) {

            processing.data.JSONObject Schedule = new processing.data.JSONObject();

            Schedule.setString("starttime", Schedules.get(i).StartTime);
            Schedule.setString("endtime", Schedules.get(i).EndTime);
            Schedule.setString("content", Schedules.get(i).Content);

            values.setJSONObject(i, Schedule);
        }
        saveJSONArray(values, "data/schedules.json");
    }
}
public class ScheduleModel
{
    //もういやだああああああああああGetterとSetter作りたくないいいいいいいいいいいいいいい
    public String StartTime = null;
    public String EndTime = null;
    public int Category = 0;
    public String Content = null;
    public ScheduleModel(String starttime, String endtime, String content)
    {
        this.StartTime = starttime;
        this.EndTime = endtime;
        this.Content = content;
        this.Category = 
        starttime == "" && endtime == "" ? 0 : //TODO
        starttime != "" && endtime == "" ? 1 : //Notify
        starttime == "" && endtime != "" ? 2 : //DeadLine
        starttime != "" && endtime != "" ? 3 : 11; //Schedule 
        //11はエラー（デバッグ用。Javaの仕様ﾜｶﾝﾈ）
    }
    public String GetShowScheduleString()
    {
        if(this.Category == 0)
        {
            return "･" + Content;
        }
        if(this.Category == 1)
        {
            return "<" + StartTime + ">" + "\n" +
                "   " + Content + "\n" +
                "</" + StartTime + ">";  
        }
        if(this.Category == 2)
        {
            return "<" + EndTime + ">" + "\n" +
                "   " + Content + "\n" +
                "</" + EndTime + ">";  
        }
        if(this.Category == 3)
        {
            return "<" + StartTime + ">" + "\n" +
                "   " + Content + "\n" +
                "</" + EndTime + ">";  
        }
        return "";
     }
}